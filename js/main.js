/*Теоретичні питання та відповіді.
    1. Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
    AJAX — це технологія веб-розробки, яка дозволяє веб-сторінці взаємодіяти з сервером асинхронно, без необхідності перезавантажувати сторінку. 
    Використання AJAX у розробці Javascript дозволяє створити більш швидкі та інтерактивні веб-додатки. 
    Це корисно для завантаження та оновлення даних на сторінці без перерви в роботі користувача, 
    а також для реалізації різноманітних функцій, таких як автозаповнення полів, завантаження даних у режимі реального часу та інше.


/*Завдання
Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

Технічні вимоги:
Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, 
а також короткий зміст (поля episodeId, name, openingCrawl).
Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.
Необов'язкове завдання підвищеної складності
Поки завантажуються персонажі фільму, прокручувати під назвою фільму анімацію завантаження. Анімацію можна використовувати будь-яку. Бажано знайти варіант на чистому CSS без використання JavaScript.
Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Література:
Використання Fetch на MDN
Fetch
CSS анімація
Події DOMM*/


const filmsContainer = document.getElementById('filmsContainer');

fetch('https://ajax.test-danit.com/api/swapi/films')
  .then(response => response.json())
  .then(data => {
    data.forEach(film => {
      const filmElement = document.createElement('div');
      filmElement.innerHTML = `
        <h2>Епізод ${film.episodeId}: ${film.name}</h2>
        <p>Короткий зміст: ${film.openingCrawl}</p>
      `;
      
      film.characters.forEach(characterUrl => {
        fetch(characterUrl)
          .then(response => response.json())
          .then(character => {
            const characterElement = document.createElement('p');
            characterElement.textContent = `- Персонаж: ${character.name}`;
            filmElement.appendChild(characterElement);
          });
      });
      
      filmsContainer.appendChild(filmElement);
    });
  })
  .catch(error => {
    console.error('Виникла помилка:', error);
  });




